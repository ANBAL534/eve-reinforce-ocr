from PIL import Image


def clean_screenshot(image):
    im = Image.open(image)
    im.save('images/image.jpg')
    pixel_map = im.load()

    i_start = int(im.size[0]/4)
    i_end = int(im.size[0]/4)*3
    j_start = int(im.size[1]/4)
    j_end = int(im.size[1]/6)*4

    img = Image.new(im.mode, (i_end-i_start, j_end-j_start))
    pixels_new = img.load()

    # black and white image
    for i in range(i_start, i_end):
        for j in range(j_start, j_end):
            if pixel_map[i, j][0] < 190 and pixel_map[i, j][1] < 190 and pixel_map[i, j][2] < 190:
                pixels_new[i-i_start, j-j_start] = (255,255,255,0)
            else:
                pixels_new[i-i_start, j-j_start] = (0,0,0,0)

    # remove corners
    for i in range(0, int(img.size[0]/3)):
        for j in range(0, int(img.size[1]/3)):
            pixels_new[i, j] = (255,255,255,0)
    for i in range(int(img.size[0]/3)*2, int(img.size[0])):
        for j in range(0, int(img.size[1]/3)):
            pixels_new[i, j] = (255,255,255,0)
    for i in range(0, int(img.size[0]/3)):
        for j in range(int(img.size[1]/3)*2, int(img.size[1])):
            pixels_new[i, j] = (255,255,255,0)
    for i in range(int(img.size[0]/3)*2, int(img.size[0])):
        for j in range(int(img.size[1]/3)*2, int(img.size[1])):
            pixels_new[i, j] = (255,255,255,0)

    # clear the image
    black_pixels_n = 0
    for i in range(i_start, i_end, 20):
        for j in range(j_start, j_end, 20):
            for i_o in range(20):
                for j_o in range(20):
                    try:
                        if 0 == pixels_new[i-i_start+i_o, j-j_start+j_o][0]:
                            black_pixels_n += 1
                    except IndexError:
                        pass
            if black_pixels_n < 25:
                for i_o in range(20):
                    for j_o in range(20):
                        try:
                            pixels_new[i-i_start+i_o, j-j_start+j_o] = (255,255,255,0)
                        except IndexError:
                            pass
            black_pixels_n = 0

    img = img.resize((img.size[0]*5, img.size[1]*5), 1)

    # img.show()
    img.save("images/mod.jpg")


if __name__ == '__main__':
    clean_screenshot('images/image2.png')
