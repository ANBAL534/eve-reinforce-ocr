import datetime
import sqlite3


class Db(object):
    def __init__(self):
        self.conn = sqlite3.connect('db.sqlite3')
        self.cur = self.conn.cursor()

        # Create tables
        self.cur.execute("CREATE TABLE IF NOT EXISTS watched_channel ("
                         "channel_id INTEGER PRIMARY KEY"
                         ")")
        self.cur.execute("CREATE TABLE IF NOT EXISTS timer ("
                         "timer_id INTEGER PRIMARY KEY,"
                         "channel_id INTEGER NOT NULL,"
                         "message_id INTEGER NOT NULL UNIQUE,"
                         "date DATETIME NOT NULL,"
                         "detected_timer_text VARCHAR(10) NOT NULL,"
                         "image_url VARCHAR(255) NOT NULL,"
                         "called BOOL NOT NULL DEFAULT 0,"
                         "cancelled BOOL NOT NULL DEFAULT 0,"
                         "FOREIGN KEY (channel_id) REFERENCES watched_channel (channel_id) ON DELETE CASCADE"
                         ")")
        self.conn.commit()

    def _get_new_cursor(self):
        conn = sqlite3.connect('db.sqlite3')
        cur = conn.cursor()
        return conn, cur

    def add_channel_to_watchlist(self, channel_id):
        try:
            self.cur.execute("INSERT INTO watched_channel VALUES (?)", (int(channel_id),))
            self.conn.commit()
        except Exception:
            return False  # TODO Too lazy to specify the sql error or the int transformation error etc.
        return True

    def remove_channel_to_watchlist(self, channel_id):
        try:
            self.cur.execute("DELETE FROM watched_channel WHERE channel_id = ?", (int(channel_id),))
            self.conn.commit()
        except Exception:
            return False  # TODO Too lazy to specify the sql error or the int transformation error etc.
        return True

    def is_channel_in_watchlist(self, channel_id):
        if len(self.cur.execute("SELECT * FROM watched_channel WHERE channel_id = ?", (int(channel_id),)).fetchall()) == 1:
            return True
        return False

    def add_timer(self, datetime_obj, left_str, message_id, channel_id, image_url):
        try:
            res = self.cur.execute("INSERT INTO timer (channel_id, message_id, date, detected_timer_text, image_url) "
                                   "VALUES (?, ?, ?, ?, ?)",
                                   (int(channel_id), int(message_id), datetime_obj, str(left_str), str(image_url)))
            self.conn.commit()
        except Exception:
            return -1  # TODO Too lazy to specify the errors
        return res.lastrowid

    def cancel_timer(self, channel_id, message_id):
        try:
            self.cur.execute("UPDATE timer SET cancelled = 1 WHERE channel_id = ? AND message_id = ?",
                             (int(channel_id), int(message_id)))
            self.conn.commit()
        except Exception:
            return False  # TODO Too lazy to specify the errors
        return True

    def called_timer(self, channel_id, message_id, new_cursor=False):
        if new_cursor:
            conn, cur = self._get_new_cursor()
        else:
            conn = self.conn
            cur = self.cur
        try:
            cur.execute("UPDATE timer SET called = 1 WHERE channel_id = ? AND message_id = ?",
                        (int(channel_id), int(message_id)))
            conn.commit()
        except Exception:
            return False  # TODO Too lazy to specify the errors
        return True

    def get_timer(self, timer_id, new_cursor=False):
        if new_cursor:
            conn, cur = self._get_new_cursor()
        else:
            conn = self.conn
            cur = self.cur
        try:
            return cur.execute("SELECT timer_id, channel_id, message_id, date, detected_timer_text, image_url, "
                               "called, cancelled FROM timer WHERE timer_id = ?", (int(timer_id),)).fetchone()
        except Exception:
            return None  # TODO Too lazy

    def get_all_pending_timers(self):
        return self.cur.execute("SELECT timer_id, channel_id, message_id, date, detected_timer_text, image_url, "
                                "called, cancelled FROM timer WHERE called = 0 AND cancelled = 0 AND date > ?",
                                (datetime.datetime.now(),)).fetchall()
