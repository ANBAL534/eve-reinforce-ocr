from discord_client import DiscordClient


def main():
    client = DiscordClient()
    client.run('your-discord-bot-key')


if __name__ == '__main__':
    main()
