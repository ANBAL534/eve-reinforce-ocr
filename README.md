# EVE Online Citadel Reinforcement Timers OCR
This application aims to implement a Discord Bot to receive screenshots from the game EVE Online
showing a reinforced citadel or ansiblex structure and to recognize through Pillow, OpenCV EAST
and Tesseract the time left of reinforcement and to create a timer to ping when the reinforcement timer ends.

### How to use

#### Prepare the enviroment
```shell
# Clone this repo
git clone https://gitlab.com/ANBAL534/eve-reinforce-ocr.git eve-reinforce-ocr
cd eve-reinforce-ocr

# Create a virtualenv to not fuck up your python install
# (I made this software using Python 3.6)
virtualenv -p /usr/bin/python3.6 venv
source venv/bin/activate

# Install requirements
pip3 install -r requirements.txt
```

#### Configuration
Add your bot's keys into `main.py`

Add your regex rules into `regex-error-fix.txt` (See examples in the file itself)

#### Usage
```shell
# With the virtualenv activated and in the same folder as the main.py
python3 main.py
```

### Common OCR error fixes
If you detect errors in the recognition of some characters you can add regex strings and the substitution it should make using this format:

```shell
[regex]->[string]
n$->h
7R->21
```