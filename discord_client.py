import asyncio
import datetime
from threading import Thread

import opencv_east
import requests
import clean_screenshot
import discord
from db import Db


class DiscordClient(discord.Client):
    def __init__(self, **options):
        super().__init__(**options)
        self.db = Db()

        # Setup pending timers on the db
        pending_timers = self.db.get_all_pending_timers()
        for timer in pending_timers:
            Thread(target=self.thread_ping, args=(datetime.datetime.strptime(timer[3], '%Y-%m-%d %H:%M:%S.%f'), timer[0])).start()

    async def on_ready(self):
        print('Logged on as', self.user)

    async def on_message(self, message):
        # don't respond to ourselves
        if message.author == self.user:
            return

        if message.content == "!start_watching" and not self.db.is_channel_in_watchlist(message.channel.id):
            if self.db.add_channel_to_watchlist(message.channel.id):
                await message.reply("Started watching this channel for screenshots!")
        elif message.content == "!stop_watching" and self.db.is_channel_in_watchlist(message.channel.id):
            if self.db.remove_channel_to_watchlist(message.channel.id):
                await message.reply("Stopped watching this channel for screenshots!")
        elif self.db.is_channel_in_watchlist(message.channel.id) and len(message.attachments) > 0 \
                and message.attachments[0].url.split(".")[-1] in ["jpg", "png", "jpeg"]:
            clean_screenshot.clean_screenshot(requests.get(message.attachments[0].url, stream=True).raw)
            (datetime_obj, left_str) = opencv_east.timer_ocr()

            timer_id = self.db.add_timer(datetime_obj, left_str, message.id, message.channel.id, message.attachments[0].url)
            if timer_id != -1:
                Thread(target=self.thread_ping, args=(datetime_obj, timer_id)).start()
                await message.channel.send("Timer set up for: {}".format(str(datetime_obj)))
            else:
                await message.channel.send("An error occurred when adding the timer to the DB!")

            sent_message = await message.reply("Timer time left: {} (Downvote if wrong to cancel the timer)".format(
                left_str), mention_author=False)
            await sent_message.add_reaction("👍")
            await sent_message.add_reaction("👎")

    async def on_raw_reaction_add(self, payload):
        # don't respond to ourselves
        if payload.user_id == self.user.id:
            return

        message = await (await self.fetch_channel(payload.channel_id)).fetch_message(payload.message_id)

        if payload.emoji.name == '👎':
            self.db.cancel_timer(message.reference.channel_id, message.reference.message_id)
            await message.channel.send("Timer cancelled")
        if payload.emoji.name == '👍' and message.reactions[0].count == 2:
            user = await self.fetch_user(payload.user_id)
            await message.channel.send("Thanks {} 🙂".format(str(user.mention)))

    async def send_ping(self, timer_id):
        timer = self.db.get_timer(timer_id, new_cursor=True)
        if timer[6] == 1 or timer[7] == 1:
            return  # Timer was cancelled or already called
        channel = await self.fetch_channel(timer[1])
        message = await channel.fetch_message(timer[2])
        await message.reply("Is about to start repairing", mention_author=False)
        self.db.called_timer(timer[1], timer[2], new_cursor=True)

    async def wait_until(self, dt):
        # sleep until the specified datetime
        now = datetime.datetime.now()
        print("Waiting for " + str((dt - now).total_seconds()))
        await asyncio.sleep((dt - now).total_seconds())

    async def run_at(self, dt, coro):
        await self.wait_until(dt)
        return await coro

    def thread_ping(self, datetime_obj, timer_id):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        asyncio.get_event_loop().run_until_complete(self.run_at(datetime_obj, self.send_ping(timer_id)))
