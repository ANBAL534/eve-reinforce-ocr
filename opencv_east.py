import datetime
import re
import clean_screenshot
import pytesseract as pytesseract
from imutils.object_detection import non_max_suppression
import numpy as np
import time
import cv2


def timer_ocr():
    # load the input image and grab the image dimensions
    image = cv2.imread("images/mod.jpg")
    orig = image.copy()
    (orig_h, orig_w) = image.shape[:2]
    # set the new width and height and then determine the ratio in change
    # for both the width and height
    (new_w, new_h) = (1600, 800)
    r_w = orig_w / float(new_w)
    r_h = orig_h / float(new_h)
    # resize the image and grab the new image dimensions
    image = cv2.resize(image, (new_w, new_h))
    (H, W) = image.shape[:2]

    # define the two output layer names for the EAST detector model that
    # we are interested -- the first is the output probabilities and the
    # second can be used to derive the bounding box coordinates of text
    layer_names = [
        "feature_fusion/Conv_7/Sigmoid",
        "feature_fusion/concat_3"]

    # load the pre-trained EAST text detector
    print("[INFO] loading EAST text detector...")
    net = cv2.dnn.readNet('frozen_east_text_detection.pb')
    # construct a blob from the image and then perform a forward pass of
    # the model to obtain the two output layer sets
    blob = cv2.dnn.blobFromImage(image, 1.0, (W, H),
                                 (123.68, 116.78, 103.94), swapRB=True, crop=False)
    start = time.time()
    net.setInput(blob)
    (scores, geometry) = net.forward(layer_names)
    end = time.time()
    # show timing information on text prediction
    print("[INFO] text detection took {:.6f} seconds".format(end - start))

    (rects, confidences) = decode_predictions(scores, geometry)

    # apply non-maxima suppression to suppress weak, overlapping bounding
    # boxes
    boxes = non_max_suppression(np.array(rects), probs=confidences)

    # initialize the list of results
    results = []

    # loop over the bounding boxes
    for (start_x, start_y, end_x, end_y) in boxes:
        # scale the bounding box coordinates based on the respective
        # ratios
        start_x = int(start_x * r_w)
        start_y = int(start_y * r_h)
        end_x = int(end_x * r_w)
        end_y = int(end_y * r_h)
        # in order to obtain a better OCR of the text we can potentially
        # apply a bit of padding surrounding the bounding box -- here we
        # are computing the deltas in both the x and y directions
        d_x = int((end_x - start_x) * 0.05)
        d_y = int((end_y - start_y) * 0.05)
        # apply padding to each side of the bounding box, respectively
        start_x = max(0, start_x - d_x)
        start_y = max(0, start_y - d_y)
        end_x = min(orig_w, end_x + (d_x * 2))
        end_y = min(orig_h, end_y + (d_y * 2))
        # extract the actual padded ROI
        roi = orig[start_y:end_y, start_x:end_x]

        # in order to apply Tesseract v4 to OCR text we must supply
        # (1) a language, (2) an OEM flag of 4, indicating that the we
        # wish to use the LSTM neural net model for OCR, and finally
        # (3) an OEM value, in this case, 7 which implies that we are
        # treating the ROI as a single line of text
        config = "-l eng --oem 2 --psm 7"
        text = pytesseract.image_to_string(roi, config=config)
        # add the bounding box coordinates and OCR'd text to the list
        # of results
        results.append(((start_x, start_y, end_x, end_y), text))

    # sort the results bounding box coordinates from top to bottom
    results = sorted(results, key=lambda r: r[0][1])

    days_left = 0
    hours_left = 0
    minutes_left = 0
    for r in results:
        string = r[1].strip()

        # Known OCR errors
        with open("regex-error-fix.txt", "r") as fixes:
            for line in fixes.readlines():
                string = re.sub(line.split("->")[0], line.split("->")[1].strip(), string)

        # Timer
        if string.endswith("d"):
            string = re.sub(r"[a-zA-Z]", "", string)
            string = re.sub(r"d$", "", string)
            print("{} days".format(string))
            days_left = int(string)
        if string.endswith("h"):
            string = re.sub(r"[a-zA-Z]", "", string)
            string = re.sub(r"h$", "", string)
            print("{} hours".format(string))
            hours_left = int(string)
        if string.endswith("m"):
            string = re.sub(r"[a-zA-Z]", "", string)
            string = re.sub(r"m$", "", string)
            print("{} minutes".format(string))
            minutes_left = int(string)

    date_time_obj = datetime.datetime.now() + datetime.timedelta(days=days_left) \
        + datetime.timedelta(hours=hours_left) + datetime.timedelta(minutes=minutes_left)
    return date_time_obj, "{}d {}h {}m".format(str(days_left), str(hours_left), str(minutes_left))


def decode_predictions(scores, geometry):
    # grab the number of rows and columns from the scores volume, then
    # initialize our set of bounding box rectangles and corresponding
    # confidence scores
    (num_rows, num_cols) = scores.shape[2:4]
    rects = []
    confidences = []
    # loop over the number of rows
    for y in range(0, num_rows):
        # extract the scores (probabilities), followed by the
        # geometrical data used to derive potential bounding box
        # coordinates that surround text
        scores_data = scores[0, 0, y]
        x_data0 = geometry[0, 0, y]
        x_data1 = geometry[0, 1, y]
        x_data2 = geometry[0, 2, y]
        x_data3 = geometry[0, 3, y]
        angles_data = geometry[0, 4, y]
        # loop over the number of columns
        for x in range(0, num_cols):
            # if our score does not have sufficient probability,
            # ignore it
            if scores_data[x] < 0.5:
                continue
            # compute the offset factor as our resulting feature
            # maps will be 4x smaller than the input image
            (offset_x, offset_y) = (x * 4.0, y * 4.0)
            # extract the rotation angle for the prediction and
            # then compute the sin and cosine
            angle = angles_data[x]
            cos = np.cos(angle)
            sin = np.sin(angle)
            # use the geometry volume to derive the width and height
            # of the bounding box
            h = x_data0[x] + x_data2[x]
            w = x_data1[x] + x_data3[x]
            # compute both the starting and ending (x, y)-coordinates
            # for the text prediction bounding box
            end_x = int(offset_x + (cos * x_data1[x]) + (sin * x_data2[x]))
            end_y = int(offset_y - (sin * x_data1[x]) + (cos * x_data2[x]))
            start_x = int(end_x - w)
            start_y = int(end_y - h)
            # add the bounding box coordinates and probability score
            # to our respective lists
            rects.append((start_x, start_y, end_x, end_y))
            confidences.append(scores_data[x])
    # return a tuple of the bounding boxes and associated confidences
    return rects, confidences


if __name__ == '__main__':
    clean_screenshot.clean_screenshot('images/image2.png')
    timer_ocr()
